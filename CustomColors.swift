//
//  CustomColor.swift
//  RSRPechHulpTest
//
//  Created by Alex Matv on 01/07/2017.
//  Copyright © 2017 Alex Matviiets. All rights reserved.
//

import UIKit

struct CustomColors {
    static let transparentColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.0);
    
}
