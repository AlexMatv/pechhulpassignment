//
//  CustomAnnotationView.swift
//  RSRPechHulpTest
//
//  Created by Alex Matv on 01/07/2017.
//  Copyright © 2017 Alex Matviiets. All rights reserved.
//

import Foundation
import MapKit

class CustomAnnotationView : MKPinAnnotationView
{
    let selectedLabel:UILabel = UILabel.init(frame:CGRect(x: 0, y: 0, width: 204, height: 172))
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(false, animated: animated)
        
        if(selected)
        {
            selectedLabel.lineBreakMode = .byWordWrapping
            selectedLabel.numberOfLines = 0
            selectedLabel.text = ((annotation?.title)!)! + "\n\n" + ((annotation?.subtitle!)!)
            selectedLabel.textAlignment = .center
            selectedLabel.textColor = .white
            selectedLabel.backgroundColor = UIColor(patternImage: UIImage(named: "address_back")!)
            selectedLabel.layer.borderColor = CustomColors.transparentColor.cgColor
            selectedLabel.layer.borderWidth = 2
            selectedLabel.layer.cornerRadius = 5
            selectedLabel.layer.masksToBounds = true
            selectedLabel.center.x = 0.5 * self.frame.size.width
            selectedLabel.center.y = -0.5 * selectedLabel.frame.height
            
            self.addSubview(selectedLabel)
            let cpa = annotation as! CustomPointAnnotation
            self.image = UIImage(named:cpa.imageName)
        }
        else
        {
            selectedLabel.removeFromSuperview()
        }
    }
}
