//
//  MapViewController.swift
//  RSRPechHulpTest
//
//  Created by Alex Matv on 20/06/2017.
//  Copyright © 2017 Alex Matviiets. All rights reserved.

import CoreLocation
import MapKit
import UIKit
import SystemConfiguration

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    let locationManager = CLLocationManager()
    let spanLatitudeDelta = 0.02
    let spanLongitudeDelata = 0.02

    @IBOutlet weak var popupWindow: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var belRSRNuButton: UIButton!
    @IBOutlet weak var belNuButton: UIButton!
    @IBOutlet weak var annulerenButton: UIButton!
    @IBOutlet weak var popupWindowTitle: UILabel!
    @IBOutlet weak var popupWindowFirstLabel: UILabel!
    @IBOutlet weak var popupWindowSecondLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isLocationEnabled()
        isConnectedToNetwork()
        
        popupWindow.isHidden = true
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    let geocoder = CLGeocoder()

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            if mapView.annotations.isEmpty {
                let region = MKCoordinateRegion(center: location.coordinate, span: MKCoordinateSpanMake(spanLatitudeDelta, spanLatitudeDelta))
            
                let currentCoordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)

                addAnnotation(for: currentCoordinate)
                mapView.setRegion(region, animated: false)
                mapView.showsUserLocation=false
            } else {
                let currentCoordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                let currentAnnotation = mapView.selectedAnnotations[0] as! CustomPointAnnotation
                currentAnnotation.setCoordinate(newCoordinate: currentCoordinate)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        
        let reuseId = "AddressAnotation"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = CustomAnnotationView.init(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = false
        }
        else {
            anView?.annotation = annotation
        }
        
        return anView
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView){
        mapView.selectAnnotation(mapView.annotations.first!, animated: false)
    }

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error)")
    }
    
    func addAnnotation(for coordinate: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            if let placemark = placemarks?.first {
                let annotation = CustomPointAnnotation()
                annotation.imageName = "marker.png"
                annotation.coordinate = coordinate
                annotation.title = NSLocalizedString("annotationTitle", comment: "comment")
                annotation.subtitle = placemark.name! + "\n" + placemark.postalCode! + ", " + placemark.locality!
                annotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                                               longitude: location.coordinate.longitude);
                self.mapView.addAnnotation(annotation)
                self.mapView.selectAnnotation(annotation, animated: false)
            }
        }
    }
    
    @IBAction func tapBelRSRNuButton(_ sender: Any) {
        popupWindow.isHidden = false
        belRSRNuButton.isHidden = true
        mapView.view(for: mapView.annotations.first!)?.isHidden = true
        if UIDevice.current.userInterfaceIdiom != .phone {
            belNuButton.isHidden = true
            popupWindowTitle.isHidden = true
            popupWindowFirstLabel.text = NSLocalizedString("telText", comment: "comment")
            popupWindowSecondLabel.text = NSLocalizedString("RSRTelNumber", comment: "comment")
        }
    }
    
    @IBAction func tapBelNuButton(_ sender: Any) {
        guard let number = URL(string: "tel://" + NSLocalizedString("RSRTelNumber", comment: "comment")) else { return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func tapAnnulerenButton(_ sender: Any) {
        popupWindow.isHidden = true
        belRSRNuButton.isHidden = false
        mapView.view(for: mapView.annotations.first!)?.isHidden = false
    }
    
    func isConnectedToNetwork() {
        if NetworkConnectionCheckManager.connectedToNetwork() == false {
        self.displayNetworkPopup()
        }
    }
    
    func isLocationEnabled() {
        if !CLLocationManager.locationServicesEnabled() {
            displayLocationPopup()
        }
    }
    
    func displayNetworkPopup() {
        let alertController = UIAlertController(title: NSLocalizedString("networkPopupTitle", comment: "comment"), message:
            NSLocalizedString("networkPopupMessage", comment: "comment"), preferredStyle: UIAlertControllerStyle.alert)
        let locationAlernAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){
            action in self.isConnectedToNetwork()
            self.isLocationEnabled()
        }
        
        alertController.addAction(locationAlernAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayLocationPopup() {
        let alertController = UIAlertController(title: NSLocalizedString("gPSPopupTitle", comment: "comment"), message:
            NSLocalizedString("gPSPopupMessage", comment: "comment"), preferredStyle: UIAlertControllerStyle.alert)
        let locationAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){
            action in self.isLocationEnabled()
            self.isConnectedToNetwork()
        }
        
        alertController.addAction(locationAlertAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
