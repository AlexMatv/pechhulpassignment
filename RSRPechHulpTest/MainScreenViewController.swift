//
//  MainScreenViewController.swift
//  RSRPechHulpTest
//
//  Created by Alex Matv on 19/06/2017.
//  Copyright © 2017 Alex Matviiets. All rights reserved.
//

import UIKit
import CoreLocation

class MianScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = NSLocalizedString("backItemTitle", comment: "comment")
        navigationItem.backBarButtonItem = backItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

