//
//  CustomPointAnnotation.swift
//  RSRPechHulpTest
//
//  Created by Alex Matv on 01/07/2017.
//  Copyright © 2017 Alex Matviiets. All rights reserved.
//

import Foundation
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
    private var coord: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    func setCoordinate(newCoordinate: CLLocationCoordinate2D) {
        self.coordinate = newCoordinate
    }
}
